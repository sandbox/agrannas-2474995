(function ($) {
  Drupal.behaviors.seat_booking_map = {
    renderMap : function (clickable, seat_map, context) {
      $('#seat_booking_svg_map', context).once('init-seat-map', function(){
          
          var svgContainer = d3.select('#seat_booking_svg_map').append('svg')
                                                               .attr('style', 'width: 100%; height: 500px;');
          
          var seatGroup = svgContainer.append("g");
          
          var seats = seatGroup.selectAll('rect')
                                  .data(seat_map)
                                  .enter()
                                  .append('rect');
                                  
          var colors = {
            "free": "green",
            "my_booked": "yellow",
            "booked": "red",
          }
          
          var seat_attribs = seats
                              .attr("x", function(d) { return d.x; })
                              .attr("y", function(d) { return d.y; })
                              .attr("width", function(d) { return d.width; })
                              .attr("height", function(d) { return d.height; })
                              .style("fill", function(d) { return colors[d.state]; });
                              
          $(window).resize(function() {
            var width = parseInt(d3.select('#seat_booking_svg_map').style('width'));
            var height = parseInt(d3.select('#seat_booking_svg_map').style('height'));
            var g_width = d3.select('#seat_booking_svg_map g').node().getBBox().width;
            var scale = width/700;
            if(scale < 0.4){
              scale = 0.4;
            }
            g_width = g_width*(scale);
            seatGroup.attr("transform", "translate(" + ((width/2)-(g_width/2)) + ", 0) scale(" + (scale) + ")");
          });
          
          if(clickable){                    
            seats.on('click', function(d) {
              if(d.state != 'booked'){
                var seat = d3.select(this);
                if(d.state == 'my_booked'){
                  d.state = "free";
                  seat.style('fill', colors[d.state]);
                }else{
                  d.state = "my_booked";
                  seat.style('fill', colors[d.state]);
                }
                
                
                $('.seat-map-seat-'+d.id, context).click(); 
              } 
              return d; 
            });
        }
      });
    },
    attach: function (context, settings) {
      if(typeof settings.seat_booking != 'undefined'){
        if(typeof settings.seat_booking.seat_map != 'undefined'){
          if(typeof settings.seat_booking.seat_map_widget != 'undefined' && settings.seat_booking.seat_map_widget){
            Drupal.behaviors.seat_booking_map.renderMap(true, JSON.parse(settings.seat_booking.seat_map), context);
          }else if(typeof settings.seat_booking.seat_map_formatter != 'undefined' && settings.seat_booking.seat_map_formatter){
            Drupal.behaviors.seat_booking_map.renderMap(false, JSON.parse(settings.seat_booking.seat_map), context);
          }
        }
      }
    }
  };
}(jQuery));