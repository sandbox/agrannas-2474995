<?php

/**
 * Extending the EntityAPIController for the boking entity.
 */
class BokingEntityController extends EntityApiController {
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity, $view_mode, $langcode, $content);
    
    $build['seats'] = array(
      '#type' => 'markup',
      '#markup' => check_plain('Seats: ' . count($entity->seats)),
      '#prefix' => '<div class="boking-seats">',
      '#suffix' => '</div>',
    );
    
    return $build;
  }
  
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    if(parent::save($entity, $transaction)){
      // Update data
      if(isset($entity->seats) && !empty($entity->seats)){
        foreach($entity->seats as $seat){
          if(isset($seat->delete) && $seat->delete){
            entity_delete('boking_seat', $seat->id);
            continue;
          }
          $seat->save();
        }
      }
      return TRUE;
    }
  }
  
  public function delete($ids){
    $enteties = entity_load('boking', $ids);
    foreach($enteties as $entity){
      foreach($entity->seats as $seat){
        entity_delete('boking_seat', $seat->id);
      }
    }
    parent::delete($ids);
  }
  
  public function load($ids = array(), $conditions = array()){
    $entities = parent::load($ids, $conditions);
    
    foreach($entities as $id => $entity){
      $query = db_select('seat_booking_seats', 's')
        ->condition('s.boking_room_id', $id, '=')
        ->fields('s', array('id'));
      $result = $query->execute();
      $seat_ids = array();
      foreach ($result as $record) {
        $seat_ids[] = $record->id;
      }
      $entity->seats = entity_load('boking_seat', $seat_ids);
    }
    return $entities;
  }
}


/**
 * Extending the Entity class
 */
class BokingEntity extends Entity {
  
  protected function defaultUri(){
    return array('path' => 'boking/' . $this->identifier());
  }

  public function addSeat(){
    $seat = entity_create('boking_seat', array(
      'boking_room_id' => $this->id,
      'x' => 0,
      'y' => 0,
      'weight' => 0,
    ));
    $seat->save();
    $this->seats[$seat->id] = $seat;
  }
}


class BokingSeatEntityController extends EntityApiController{
}

class BokingSeatEntity extends Entity {
  protected function defaultUri(){
    return array('path' => 'bokingseat/' . $this->identifier());
  }
}
