<?php

/**
 * Entity metadata Controller
 */
class BookingMetadataController extends EntityDefaultMetadataController {
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $info[$this->type]['properties']['id'] = array(
      'label' => t('Room id'),
      'description' => t('The id of the room'),
      'type' => 'integer',
      'schema field' => 'id',
    );
    
    $info[$this->type]['properties']['name'] = array(
      'label' => t('Room name'),
      'description' => t('The name of the room'),
      'type' => 'text',
      'schema field' => 'name',
    );
    
    return $info;
  }
}

class BookingSeatMetadataController extends EntityDefaultMetadataController {
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    
    $info[$this->type]['properties']['id'] = array(
      'label' => t('Seat id'),
      'description' => t('The id of the seat'),
      'type' => 'integer',
      'schema field' => 'id',
    );
    
    $info[$this->type]['properties']['boking_room_id'] = array(
      'label' => t('Room id'),
      'description' => t('The id of the room that this seat is associated with'),
      'type' => 'boking',
      'schema field' => 'boking_room_id',
    );
    
    $info[$this->type]['properties']['name'] = array(
      'label' => t('Seat name'),
      'description' => t('The seats name'),
      'type' => 'text',
      'schema field' => 'name',
    );
    
    $info[$this->type]['properties']['x'] = array(
      'label' => t('Seats x value'),
      'description' => t('The x value of this seat'),
      'type' => 'integer',
      'schema field' => 'x',
    );
    
    $info[$this->type]['properties']['y'] = array(
      'label' => t('Seats y value'),
      'description' => t('The y value of this seat'),
      'type' => 'integer',
      'schema field' => 'y',
    );
    
    $info[$this->type]['properties']['weight'] = array(
      'label' => t('Seats weight value'),
      'description' => t('The weight value of this seat'),
      'type' => 'integer',
      'schema field' => 'weight',
    );
    
    return $info;
  }
}
