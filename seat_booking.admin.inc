<?php

function boking_form($form, &$form_state, $boking = NULL) {
  // $form['#tree'] = TRUE;
  
  $form['name'] = array(
    '#title' => t('Boking room name'),
    '#type' => 'textfield',
    '#default_value' => isset($boking->name) ? $boking->name : '',
    '#required' => TRUE,
  );
  
  field_attach_form('boking', $boking, $form, $form_state);
  
  if(isset($boking->seats)){
    $form['seat'] = array(
      '#theme' => 'boking_form_table',
      '#header' => array(t('Id'), t('Name'), t('Seat x position'), t('Seat y position'), t('Weight'), t('Delete')),
      'rows' => array('#tree' => TRUE),
    );
    foreach($boking->seats as $key => $seat) {
      if(isset($seat->delete) && $seat->delete) continue;
      $form['seat']['rows'][$key]['id'] = array(
        '#type' => 'textfield',
        '#value' => isset($seat->id) ? $seat->id : '',
        '#size' => 2
      );
      $form['seat']['rows'][$key]['name'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($seat->name) ? $seat->name : '',
        '#size' => 20,
      );
      $form['seat']['rows'][$key]['x'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($seat->x) ? $seat->x : '',
        '#size' => 4,
      );
      
      $form['seat']['rows'][$key]['y'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($seat->y) ? $seat->y : '',
        '#size' => 4,
      );
      $form['seat']['rows'][$key]['weight'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($seat->weight) ? $seat->weight : '',
        '#size' => 2,
        '#attributes' => array('class' => array('row-weight')),
      );
      $form['seat']['rows'][$key]['delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete'),
        '#submit' => array('boking_form_delete_seat'),
        '#name' => 'delete-'.$key,
      );
    }
  }
  
  $form['add_seat'] = array(
    '#type' => 'submit',
    '#value' => t('Add another seat'),
    '#submit' => array('boking_form_add_seat'),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  
  
  return $form;
}

function boking_form_validate($form, &$form_state){
  $form_state['values']['name'] = check_plain($form_state['values']['name']);
  if(isset($form_state['values']['rows']) && !empty($form_state['values']['rows'])){
    foreach($form_state['values']['rows'] as $key => $seat){
      if(!is_numeric($seat['x'])){
        form_set_error('rows][' . $key . '][x', t('The x position can only be a number.'));
      }
      if(!is_numeric($seat['y'])){
        form_set_error('rows][' . $key . '][y', t('The y position can only be a number.'));
      }
      if(!is_numeric($seat['weight'])){
        form_set_error('rows][' . $key . '][weight', t('The weight position can only be a number.'));
      }
    }
  }
}

function boking_form_submit($form, &$form_state){
  $boking = $form_state['boking'];
  $boking->name = $form_state['values']['name'];
  if(isset($form_state['values']['rows']) && !empty($form_state['values']['rows'])){
    foreach($form_state['values']['rows'] as $key => $seat){
      $boking->seats[$key]->name = $seat['name'];
      $boking->seats[$key]->id = $seat['id'];
      $boking->seats[$key]->x = $seat['x'];
      $boking->seats[$key]->y = $seat['y'];
      $boking->seats[$key]->weight = $seat['weight'];
    }
  }
  $boking->save();
}

function boking_form_delete_seat($form, &$form_state){
  $delete_key = $form_state['triggering_element']['#parents'][1];
  $form_state['boking']->seats[$delete_key]->delete = TRUE;
  $form_state['rebuild'] = TRUE;
}

function boking_form_add_seat($form, &$form_state){
  if(isset($form_state['boking']->is_new) && $form_state['boking']->is_new) boking_form_submit($form, $form_state);
  $form_state['boking']->addSeat();
  $form_state['rebuild'] = TRUE;
}