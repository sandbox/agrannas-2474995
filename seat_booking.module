<?php

/**
 * Implements hook_entity_info
 */
function seat_booking_entity_info(){
  $info = array();
  
  $info['boking'] = array(
    'label' => t('Boking room'),
    'base table' => 'seat_booking_boking_rooms',
    'entity keys' => array(
      'id' => 'id',
      'label' => 'name',
    ),
    'entity class' => 'BokingEntity',
    'controller class' => 'BokingEntityController',
    'uri callback' => 'entity_class_uri',
    'access callback' => 'seat_booking_access',
    'admin ui' => array(
      'path' => 'admin/boking',
      'file' => 'seat_booking.admin.inc',
      'controller class' => 'EntityDefaultUiController',
    ),
    'metadata controller class' => 'BookingMetadataController',
    'module' => 'seat_booking',
  );
  
  $info['boking_seat'] = array(
    'label' => t('Boking seat'),
    'base table' => 'seat_booking_seats',
    'entity keys' => array(
      'id' => 'id',
      'label' => 'name',
    ),
    'access' => 'seat_booking_access',
    'entity class' => 'BokingSeatEntity',
    'controller class' => 'BokingSeatEntityController',
    'metadata controller class' => 'BookingSeatMetadataController',
    'uri classback' => 'entity_class_uri',
    'module' => 'seat_booking',
  );
  
  return $info;
}

function seat_booking_access($op, $entity = NULL, $account = NULL){
  if(user_access('administer seat booking', $account)){
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_libraries_info
 */
function seat_booking_libraries_info(){
  $libraries = array();
  $libraries['d3'] = array(
    'name' => 'D3 js library',
    'vendor url' => 'http://d3js.org/',
    'download url' => 'http://d3js.org/',
    'version arguments' => array(
      'file' => 'd3.js',
      'pattern' => '/version: "(\d+\.+\d+\.+\d+)"/',
      'lines' => 5,
    ),
    'files' => array(
      'js' => array('d3.min.js'),
    ),
  );
  return $libraries;
}

/**
 * Implements hook_permission
 */
function seat_booking_permission(){
  return array(
    'administer seat booking' => array(
      'title' => 'Administer booking rooms and seats',
    )
  );
}

/**
 * Implements hook_theme
 */
function seat_booking_theme() {
  return array(
    'boking_form_table' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Changes the booking room form and makes the seats into a table with tabledrag enabled.
 */
function theme_boking_form_table(&$variables) {
  $form = $variables['form'];
  $rows = $form['rows'];
  $header = $form['#header'];
  drupal_add_tabledrag('seat_booking_seat_table', 'order', 'sibling', 'row-weight');
  
  $content = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => array(),
    '#attributes' => array('id' => 'seat_booking_seat_table'),
  );
  foreach (element_children($rows) as $row_index) {
    $row = array();
    foreach (element_children($rows[$row_index]) as $col_index) {
      $row['data'][] = drupal_render($rows[$row_index][$col_index]);
      $row['class'] = array('draggable');
    }
    $content['#rows'][] = $row;
  }
  return theme('table', $content);
}

/**
 * Implements hook_field_widget_info
 */
function seat_booking_field_widget_info(){
  return array(
    'seat_booking_map_selector' => array(
      'label' => t('Seat Booking map selector'),
      'field types' => array('entityreference'),
    ),
  );
}

/**
 * Implements hook_field_widget_form
 * Creates the seat map field form.
 */
function seat_booking_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Load in the d3 library so that it can be used to draw the map.
  libraries_load('d3');
  // Load the script that draws the seat map.
  drupal_add_js(drupal_get_path('module', 'seat_booking') . '/scripts/seat_booking_map.js');
  
  if ($field['cardinality'] > 1) {
    $element['#type'] = 'fieldset';
  }
  
  // Load upp all booking rooms.
  $booking_rooms = entity_load('boking');
  
  // Save the form state values for this field incase they are set. ex. ajax call.
  if(isset($form_state['values'])){
    $form_state_values = $form_state['values'][$field['field_name']][$langcode][$delta];
  }
  
  $form_state['seat_booking_field'] = array(
    'field_name' => $field['field_name'],
    'lang' => $langcode,
    'delta' => $delta,
  );
  
  $options = array(t('Select a room...'));
  foreach($booking_rooms as $id => $boking){
    $options[$id] = $boking->name;
  }
  
  // Add the options to select a booking room for the seat map.
  $element += array(
    '#type' => 'fieldset',
    'target_id' => array(
      '#type' => 'select',
      '#title' => t('Room'),
      '#options' => $options,
      '#ajax' => array(
        'callback' => 'seat_booking_field_widget_form_ajax',
        'wrapper' => 'seat_booking_widget_ajax_wrapper',
        'method' => 'replace',
      ),
      '#default_value' => (isset($items[$delta]) ? $items[$delta] : 0),
      '#description' => t('Select a room to book a seat in'),
    ),
    'seats' => array(
      '#type' => 'fieldset',
      '#title' => t('Select a seat'),
      '#prefix' => '<div id="seat_booking_widget_ajax_wrapper">',
      '#suffix' => '</div>',
    ),
  );
  
  $registration = $form['#entity'];
  
  // Checks if a room has been selected.
  if((isset($form_state_values['target_id']) && !empty($form_state_values['target_id'])) || !empty($items[$delta])){
    
    // Get the selected rooms id.
    $room_id = (empty($form_state_values['target_id']) ? $items[$delta]['target_id'] : $form_state_values['target_id']);
    // Load in all seats associated with the room
    $seats = entity_load('boking', array($room_id));
    $seats = array_shift($seats)->seats;
    $map_settings = array();
    foreach($seats as $id => $seat){
      // Check if seat is reserved
      $seat_reservation = seat_booking_is_seat_reserved($registration->entity_type, $registration->entity_id, $id);
      // Add a hidden checkbox for the seat
      $state = 'free';
      if($seat_reservation['registration_id'] == $registration->registration_id && !empty($seat_reservation)){
        $state = 'my_booked';
      }else if(!empty($seat_reservation)){
        $state = 'booked';
      }
      
      $element['seats'][$id] = array(
        '#type' => 'checkbox',
        '#attributes' => array('class' => array("seat-map-seat-$id"), 'style' => 'display: none;'),
        '#disabled' => ($state != 'booked') ? FALSE : TRUE,
        '#default_value' => ($state == 'my_booked') ? 1 : 0,
      );
      
      // Create the d3 object settings for rendering the seat.
      $map_settings[] = array(
        'x' => $seat->x,
        'y' => $seat->y,
        'id' => $seat->id,
        'state' => $state,
        'width' => 30,
        'height' => 30,
      );
    }
    // Add the object settings to the drupal settings array. And tell that it should render for widget.
    drupal_add_js(array('seat_booking' => array('seat_map_widget' => TRUE, 'seat_map' => json_encode($map_settings))), 'setting');
    
    // Container to create the rendered seat map in.
    $element['seats']['map'] = array(
      '#markup' => '<div id="seat_booking_svg_map"></div>',
    );
  }
  
  return $element;
}

/**
 * Ajax callback for the widget form
 *  @return
 *    Returns the seats from part of the field widget form.
 */
function seat_booking_field_widget_form_ajax($form, &$form_state){
  // Get the widget from part
  $parents = $form_state['triggering_element']['#array_parents'];
  array_pop($parents);
  $element = drupal_array_get_nested_value($form, $parents);
  
  return $element['seats'];
}

/**
 * Implements hook_form_alter
 * Alters the registration form to use custom validation to validate that no booked seat has been selected.
 */
function seat_booking_form_alter(&$form, &$form_state, $form_id){
  // Only add custom validation if theres a seatmap field.
  if($form_id == 'registration_form' && isset($form_state['seat_booking_field'])){
    $form_state['storage']['validate_parent'] = $form['#validate'][0];
    $form['#validate'][0] = 'seat_booking_registration_form_validate';
  }
}

/**
 * Implements hook_form_validate
 * Custom validation function for registration forms that makes sure no booked seats are selected.
 */
function seat_booking_registration_form_validate($form, &$form_state){
  $form_state['storage']['validate_parent']($form, $form_state);
  $field = $form_state['seat_booking_field'];
  $element_values = $form_state['values'][$field['field_name']][$field['lang']][$field['delta']];
  $registration = $form['#entity'];
  
  if( isset( $element_values['seats'] ) && !empty( $element_values['seats'] ) ){
    $selected_seats = array();
    $seats_to_unreserve = array();
    foreach($element_values['seats'] as $seat_id => $selected){
      
      $seat = entity_load('boking_seat', array($seat_id));
      $seat = array_shift($seat);
      
      if($selected){
        
        $selected_seats[$seat_id] = $seat->name;
        $seat_reservation = seat_booking_is_seat_reserved($registration->entity_type, $registration->entity_id, $seat_id);
        if(!empty($seat_reservation)){
          if($seat_reservation['registration_id'] != $registration->registration_id){
            form_set_error($field['field_name'] . '][' . $field['lang'] . '][' . $field['delta'] . "][seats][$seat_id", t('The seat you have selected have already been booked'));
          }else{
            unset($selected_seats[$seat_id]);
          }
        }
      }else{
        $seat_reservation = seat_booking_is_seat_reserved($registration->entity_type, $registration->entity_id, $seat_id);
        
        if(!empty($seat_reservation)){
          $seats_to_unreserve[$seat_id] = $seat->name;
        }
      }
    }
    
    $reserved_seats = seat_booking_get_reservations_for_registration($registration->entity_type, $registration->entity_id, $registration->registration_id);
    
    
    
    // Add the seats that should be save to the registration entity.
    $form_state['registration']->seats_to_save = $selected_seats;
    $form_state['registration']->seats_to_delete = $seats_to_unreserve;
  }
}

/**
 * Implements hook_entity_insert
 * Saves reserved seats.
 */
function seat_booking_entity_insert($entity, $type){
  if($type == 'registration' && isset($entity->seats_to_save)){
    foreach ($entity->seats_to_save as $seat_id => $seat_name) {
      seat_booking_reserve_seat($entity->registration_id, $entity->entity_id, $entity->entity_type, $seat_id);
    }
    module_invoke_all('seat_booking_seats_reserved', $entity);
  }
}

/**
 * Implements hook_entity_update
 * Saves changes to seat reservations
 */
function seat_booking_entity_update($entity, $type){
  if($type == 'registration' && isset($entity->seats_to_save)){
    foreach ($entity->seats_to_save as $seat_id => $seat_name) {
      seat_booking_reserve_seat($entity->registration_id, $entity->entity_id, $entity->entity_type, $seat_id);
    }
    foreach ($entity->seats_to_delete as $seat_id => $seat_name) {
      seat_booking_remove_seat_reservation($entity->registration_id, $seat_id);
    }
    module_invoke_all('seat_booking_seats_reserved', $entity);
  }
}

/**
 * Implements hook_entity_delete
 * Removes reservations if registration is deleted.
 */
function seat_booking_entity_delete($entity, $type){
  if($type == 'registration'){
    seat_booking_remove_reservation($entity->registration_id);
  }
}

/**
 * Determines if seat has been reserved
 *  @param $enttiy_type
 *    The entity type that the registration is associated with
 *  @param $entity_id
 *    The id of the enttiy that the registration is associated with
 *  @param $seat_id
 *    The id of the seat to check for
 *  @return
 *    Returns an empty array if seat isnt reserver otherwise the registration id that reserved the seat is returned.
 */
function seat_booking_is_seat_reserved($entity_type, $entity_id, $seat_id){
  $query = db_select('seat_booking_reserved_seats', 's')
    ->condition('s.entity_id', $entity_id)
    ->condition('s.entity_type', $entity_type)
    ->condition('s.seat_id', $seat_id)
    ->fields('s', array('registration_id'))
    ->range(0, 1);
  $result = $query->execute();
  return $result->fetchAssoc();
}

/**
 * Get which room a registration was made for
 *  @param $registration_id
 *    The id of the registration
 *  @return
 *    The booking room id.
 */
function seat_booking_get_associated_room($registration_id){
  $query = db_select('seat_booking_reserved_seats', 's')
    ->condition('s.registration_id', $registration_id)
    ->fields('s', array('room_id'))
    ->range(0, 1);
  $result = $query->execute();
  return $result->fetchAssoc();
}

/**
 * Get all seat reservations for a registration
 *  @param $entity_type
 *    The entity type that the registration is associated with
 *  @param $entity_id
 *    The id of the entity that the registration is associated with
 *  @param $registration_id
 *    The id of the registration
 *  @return
 *    And array containing the room id and seat id for all reserved seats
 */
function seat_booking_get_reservations_for_registration($entity_type, $entity_id, $registration_id){
  $query = db_select('seat_booking_reserved_seats', 's')
    ->condition('s.entity_id', $entity_id)
    ->condition('s.entity_type', $entity_type)
    ->condition('s.registration_id', $registration_id)
    ->fields('s', array('room_id', 'seat_id'));
    $result = $query->execute();
    return $result->fetchAll();
}

/**
 * Reserves as seat
 *  @param $registration_id
 *    The id for the registration
 *  @param $entity_id
 *    The id of the entity that the registration is associated with
 *  @param $entity_type
 *    The entity type that the registration is associated with
 *  @param $seat_id
 *    The seats id
 */
function seat_booking_reserve_seat($registration_id, $entity_id, $entity_type, $seat_id){
  $seat = entity_load('boking_seat', array($seat_id));
  $seat = array_shift($seat);
  db_insert('seat_booking_reserved_seats')
    ->fields(array(
      'registration_id' => $registration_id,
      'room_id' => $seat->boking_room_id,
      'entity_id' => $entity_id,
      'entity_type' => $entity_type,
      'seat_id' => $seat_id,
    ))
    ->execute();
}

/**
 * Removes a seat reservation
 *  @param $registration_id
 *    The registrations id
 */
function seat_booking_remove_reservation($registration_id){
  db_delete('seat_booking_reserved_seats')
    ->condition('registration_id', $registration_id)
    ->execute();
}

function seat_booking_remove_seat_reservation($registration_id, $seat_id){
  db_delete('seat_booking_reserved_seats')
    ->condition('registration_id', $registration_id)
    ->condition('seat_id', $seat_id)
    ->execute();
}

/**
 * 
 */
function seat_booking_get_seats_for_field($field, $instance){
  if (!$options = entityreference_get_selection_handler($field, $instance)->getReferencableEntities()) {
    return array();
  }

  // Rebuild the array, by changing the bundle key into the bundle label.
  $target_type = $field['settings']['target_type'];
  $entity_info = entity_get_info($target_type);

  $return = array();
  foreach ($options as $bundle => $entity_ids) {
    $bundle_label = check_plain($entity_info['bundles'][$bundle]['label']);
    $return[] = $entity_ids;
  }
  return $return;
}

function seat_booking_field_formatter_info() {
  return array(
    'seat_booking_map_formatter' => array(
      'label' => t('Svg map formatter'),
      'field types' => array('entityreference'),
    ),
  );
}

function seat_booking_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays){
  foreach($displays as $id => $display){
    switch ($display['type']){
      case 'seat_booking_map_formatter':
        $room_id = array_shift($items[$id])['target_id'];
        $map_settings = array();
        $seats = entity_load('boking', array($room_id));
        $seats = array_shift($seats)->seats;
        $entity = $entities[$id];
        foreach($seats as $seat){
          $seat_reservation = seat_booking_is_seat_reserved($entity->entity_type, $entity->entity_id, $seat->id);
          
          $state = 'free';
          if(isset($seat_reservation['registration_id']) && $seat_reservation['registration_id'] == $entity->registration_id){
            $state = 'my_booked';
          }else if(!empty($seat_reservation)){
            $state = 'booked';
          }
          
          $map_settings[] = array(
            'x' => $seat->x,
            'y' => $seat->y,
            'id' => $seat->id,
            'state' => $state,
            'width' => 30,
            'height' => 30,
          );
        }
        
        $items[$id][0] = $map_settings;
        break;
    }
  }
}

function seat_booking_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'seat_booking_map_formatter':
      
      libraries_load('d3');
      drupal_add_js(drupal_get_path('module', 'seat_booking') . '/scripts/seat_booking_map.js');
      $map_settings = array();
      foreach($items as $delta => $item){
        $element[$delta] = array(
          '#type' => 'markup',
          '#markup' => '<div id="seat_booking_svg_map"></div>'
        );
        $map_settings = $item;
      }
      
      drupal_add_js(array('seat_booking' =>array('seat_map_formatter' => TRUE, 'seat_map' => json_encode($map_settings))), 'setting');
      break;
      
  }

  return $element;
}